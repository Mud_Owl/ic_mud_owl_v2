#!/usr/bin/python
import mraa
import time

#Classe criada para ajudar na hora da leitura de medidas
#O objeto dessa classe contem o id do registrador e o id da conversao necessaria para gerar o numero proporcional

class Dado(object):

	def __init__(self,idReg,idConv):
		self.idReg = idReg
		self.idConv = idConv

	def set_idReg(self, idReg):
		self.idReg = idReg

	def set_idConv(self, idConv):
		self.idConv = idConv
	
	def get_idReg(self, idReg):
		return self.idReg

	def get_idConv(self, idConv):
		return self.idConv

idcoff = Dado(1,2)
ign = Dado(2,3)
vdcoff = Dado(3,2)
vgn = Dado(4,3)
corrente = Dado(7,2)
tensao = Dado(8,2)
pot = Dado(9,2)
pot_ativa = Dado(10,2)
corrente_RMS = Dado(11,0)
tensao_RMS = Dado(12,0)
epsilon = Dado(13,2)
poff = Dado(14,2)
iacoff = Dado(16,2)
vacoff = Dado(17,2)
temp = Dado(19,1)
pot_reat_med = Dado(20,2)
pot_reat = Dado(21,2)
corrente_pico = Dado(22,2)
tensao_pico = Dado(23,2)
pot_reat_trian = Dado(24,4)
fator_pot = Dado(25,2)
pot_apar = Dado(27,4)
pot_apar_harm = Dado(29,2)
pot_fund_ativa = Dado(30,2)
pot_fund_reat = Dado(31,2)

#Dicionario de objetos do tipo Dado visando a facilidade na hora de se medir algum valor
global colecao 
colecao = {}
colecao['IdCoff'] = idcoff
colecao['Ign'] = ign
colecao['VdCoff'] = vdcoff
colecao['Vgn'] = vgn
colecao['Corrente'] = corrente
colecao['Tensao'] = tensao
colecao['Potencia'] = pot
colecao['Potencia Ativa'] = pot_ativa
colecao['Corrente RMS'] = corrente_RMS
colecao['Tensao RMS'] = tensao_RMS
colecao['Epsilon'] = epsilon
colecao['Poff'] = poff
colecao['IAcOff'] = iacoff
colecao['VAcOff'] = vacoff
colecao['Temperatura'] = temp
colecao['Pot Reat Med'] = pot_reat_med
colecao['Potencia Reativo'] = pot_reat
colecao['Corrente Pico'] = corrente_pico
colecao['Tensao Pico'] = tensao_pico
colecao['Pot Reat Tri'] = pot_reat_trian
colecao['Fator Potencia'] = fator_pot
colecao['Potencia Aparente'] = pot_apar
colecao['Pot Apar Harm'] = pot_apar_harm
colecao['Pot Fund Ativa'] = pot_fund_ativa
colecao['Pot fund Reativa'] = pot_fund_reat
