#!/usr/bi/python

#A parte do SPI e do SS esta declarado na classe abaixo

from Inicializacao import *
import mraa 
import time

#Dicionario com o nome dos comando e o numero deste
global comando_dic
comando_dic = {}
comando_dic['Idc offset'] = 201
comando_dic['Idc gain'] = 202
comando_dic['Iac offset'] = 205
comando_dic['Iac gain'] = 206
comando_dic['Vdc offset'] = 209
comando_dic['Vdc gain'] = 210
comando_dic['Vac offset'] = 213
comando_dic['Vac gain'] = 214
comando_dic['IVdc offset'] = 217
comando_dic['IVdc gain'] = 218
comando_dic['IVac offset'] = 221
comando_dic['IVac gain'] = 222
comando_dic['Ciclo unico'] = 224
comando_dic['Ciclo continuo'] = 232
comando_dic['Acordar'] = 160
comando_dic['Software Reset'] = 128
comando_dic['Stand-by'] = 136
comando_dic['Sleep'] = 144

#Classe responsavel pelo comandos
#O comando precisa ser passado por um hexadecimal ou decimal

class Comando(object):

#Construtor da classe Comando

	def __init__(self,comando):
		self.comando = comando_dic[comando]

#Metodo de escrita do comando
#Existe a necessidade de utilizar bytearray para garantir que o valor passado de comando sera um byte
#A parte relacionada com o SS e o SPI e global declarado no script Inicializacao (obs : talvez mude)

	def escrita_comando(self):
		self.comando = self.comando
		txbuf = bytearray(0)
		txbuf = self.comando
		ss.write(0)
		spi.writeByte(txbuf)
		ss.write(1)
		time.sleep(0.1)


