#!/usr/bin/python
import mraa 
import time

#Area de constantes
#Constante igual a 2^24 - 1
global CONSTANTE_1
CONSTANTE_1 = 16777215.0
#Constante igual a 2^16 - 1
global CONSTANTE_2
CONSTANTE_2 = 65535.0
#Constante igual a 2^22 - 1
global CONSTANTE_3
CONSTANTE_3 = 4194303.0
#Constante igual a 2^23 - 1
global CONSTANTE_4
CONSTANTE_4 = 8388607.0

#Classe que regem as conversoes 
class Conversao:
#Metodo de inicializacao
	def __init__(self, idConv, valor):
		self.idConv = idConv
		self.valor = valor

#Metodo que realiza a conversao
	def conversao(self):
#Parte responsavel pela conversao dos valores RMS
#O valor do registrador varia entre 0 e 1 e faz-se a seguinte operacao par se descobrir o valor:
#valor / CONSTANTE_1
		if self.idConv == 0:
#			print("Conversoes RMS")
			return (self.valor / CONSTANTE_1)
#Parte responsavel pela conversao da temperatura
#O valor do registrador varia entre -128 e 128.
		elif self.idConv == 1:
#			print("Conversao de temperatura")
			p_inteira = (self.valor >> 16)
			p_fracion = self.valor & 0x00FFFF
			if ((self.valor & 0x800000) == 0):
				return p_inteira + (p_fracion / CONSTANTE_2)
			else: 
				return p_inteira + (p_fracion / CONSTANTE_2) - 128
#Parte responsavel pela maioria das convercoes
#O valor do registrador varia de -1 a 1 e faz-se a seguinte operacao para se descobrir o valor:
#valor / CONSTANTE_4
		elif self.idConv == 2:
#			print("Conversao Geral")
			if self.valor < 8388608:
				return self.valor/CONSTANTE_4
			else:
				valor = self.valor & 0x7FFFFF
				return (valor/CONSTANTE_4) - 1			
#Parte responsavel por gerar o valor dos registradores de ganho
#O valor varia entre 0 e 3.999.
		elif self.idConv == 3:
#			print("Conversao Ganho")
			p_inteira = self.valor & 0xC00000
			p_fracion = self.valor & 0x3FFFFF
			p_inteira = (p_inteira >> 20)
			return p_inteira + (p_fracion/CONSTANTE_3)
#O valor do registrador varia de 0 a 1 e faz-se a seguinte operacao para se descobrir o valor:
#valor / CONSTANTE_4
		elif self.idConv == 4:
#			print self.valor
			return self.valor/CONSTANTE_4
#Caso se coloque um valor inexistente de conversao
		else:
#			print("Id invalido")
			return -1
