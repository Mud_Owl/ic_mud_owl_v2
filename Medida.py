#!/usr/bin/python
import mraa 
import time
from Registrador import *
from Conversao import *
from Dado import *
from Inicializacao import *

global colecao
#Classe responsavel por medir algo
#Faz interface entre as classe Registrador e Conversao
#Alem disso se utiliza do dicionario colecao para realizar a operacao de medicao

class Medida:
#Construtor que se utiliza da chave do dicionario para se descobrir os valores necessarios para medicao
	idReg = 0
	idConv = 0
	def __init__(self, chave):
		self.idReg = colecao[chave].idReg
		self.idConv = colecao[chave].idConv

#Metodo de medicao que junta tudo 
	def medida(self):
		aux = bytearray(3)
		aux[0] = 0xFF
		aux[1] = 0xFF
		aux[2] = 0xFF
		reg = Registrador(self.idReg,aux)
		valor = reg.leitura()
		conv = Conversao(self.idConv, valor)
		medida = conv.conversao()
#		print(medida)
		return medida		

