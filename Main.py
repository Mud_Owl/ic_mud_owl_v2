#!/usr/bin/python
from datetime import datetime
import json
import mraa 
from Medida import *
from Dado import *
from Conversao import Conversao
from Registrador import *
from Inicializacao import *
from Comando import *
from Energia import *
import csv
from Tarifacao import *
import os
global ss
global spi
global reset
global rele

spi = mraa.Spi(0)
spi.lsbmode(False)
spi.mode(mraa.SPI_MODE0)
spi.frequency(1000000)

ss = mraa.Gpio(10)
ss.dir(mraa.DIR_OUT)

reset = mraa.Gpio(8)
reset.dir(mraa.DIR_OUT)

ss.write(0)
reset.write(0)
time.sleep(0.1)
reset.write(1)
ss.write(1)

#Para a leitura de valor utilizar a classe Medida e o metodo medida
#Para a leitura de registrador puro usar a classe Registrador e o metodo leitura
#Para a escrita de registrador puro usar a classe Registrador e o metodo escrita
#Para a escrita de um comando usar a classe Comando e o metodo escrita_comando
#Para a manipulacao do reset em hardware usar a variavel global
#Valores que serao escritos precisar estar em bytearrays

#Parte de configuracao do CI
#Configuracao do registrador de configuracao
buf = bytearray(0)
txbuf = bytearray(3)
buf = 0
txbuf[0] = 0
txbuf[1] = 0
txbuf[2] = 1
valor = Registrador(buf,txbuf)
valor.escrita()
#Configuracao do registrador de mascara
buf1 = bytearray(0)
txbuf1 = bytearray(3)
buf1 = 26
txbuf1[0] = 0
txbuf1[1] = 0
txbuf1[2] = 0
valor1 = Registrador(buf1,txbuf1)
valor1.escrita()
#Configuracao do registrador de mode
buf2 = bytearray(0)
txbuf2 = bytearray(3)
buf2 = 18
txbuf2[0] = 0
txbuf2[1] = 0
txbuf2[2] = 97
valor2 = Registrador(buf2,txbuf2)
valor2.escrita()
#Configuracao do registrador de controle
buf3 = bytearray(0)
txbuf3 = bytearray(3)
buf3 = 28
txbuf3[0] = 0
txbuf3[1] = 0
txbuf3[2] = 4
valor3 = Registrador(buf3,txbuf3)
valor3.escrita()
god  = Comando('Ciclo continuo')
god.escrita_comando()

escala = json.loads(open('escala.json').read())
escala_tensao_RMS = escala['Tensao_RMS']
escala_corrente_RMS = escala['Corrente_RMS']

cliente = paho.Client()
cliente.connect("iot.eclipse.org")

while True:
	tensao = Medida('Tensao RMS')
	tensao_rede = tensao.medida()
	print 'Tensao rede'
#	print tensao_rede 
	tensao_rede1 = ((tensao_rede * escala_tensao_RMS) / 0.999999999)
	print tensao_rede1
	
	corrente = Medida('Corrente RMS')
	corrente_rede = corrente.medida()
	print 'Corrente'
#	print corrente_rede
	corrente_rede1 = ((corrente_rede * escala_corrente_RMS)/0.999999999)
	print corrente_rede1

	pot_at = Medida('Potencia Ativa')
	potencia_ativa = pot_at.medida()
	print 'Potencia'
	potencia_ativa1 = ((potencia_ativa * (escala_corrente_RMS * escala_tensao_RMS))/ (0.999999999 * 0.999999999))
	print potencia_ativa1

	pot_ap = Medida('Potencia Aparente')
	potencia_aparente = pot_ap.medida()
	print 'Potencia Aparente'
	potencia_aparente1 = ((potencia_aparente * (escala_tensao_RMS * escala_corrente_RMS))/ (0.999999999 * 0.999999999))
	print potencia_aparente1

	fator = Medida('Fator Potencia')
	fp = fator.medida()
	print 'Fator de Potencia'
	print fp

	frequencia = Medida('Epsilon')
	freq = frequencia.medida()
	print 'Frequencia'
	real_freq =freq * 4000
	print real_freq
	
	pot_reat = Medida('Pot Reat Med')
	potencia_reativa = pot_reat.medida()
	print 'Potencia Reativa'
	potencia_reativa1 = ((potencia_reativa * (escala_tensao_RMS * escala_corrente_RMS))/ (0.999999999 * 0.999999999))
	print potencia_reativa1

	payload =	{
			'tensao_rms':tensao_rede1,
			'corrente_rms':corrente_rede1,
			'potencia_ativa':potencia_ativa1,
			'potencia_aparente':potencia_aparente1,
			'fator_potencia':fp,
			'frequencia':real_freq,
			'potencia_reativa':potencia_reativa1
			}
	lol = str(json.dumps(payload))
	cliente.publish(topic="dash/dev1",payload=lol)
	#print(lol)
	time.sleep(1.0)
	os.system('cls' if os.name == 'nt' else 'clear')
cliente.disconnect()	
