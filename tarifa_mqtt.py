#!/usr/bin/python
import mraa
from Medida import *
from datetime import datetime
import json
from Inicializacao import *
import csv
import paho.mqtt.client as paho
import sys
import logging
import time
import getopt
global rele

import paho.mqtt.client as paho
from Dado import *
from Conversao import Conversao
from Registrador import *
from Inicializacao import *
from Comando import *

############### Testar em breve ###################
# Montar uma main so para isso
def energia_segundo_hora():
	escala = json.loads(open('escala.json').read())
	escala_tensao_RMS = escala['Tensao_RMS']
	escala_corrente_RMS = escala['Corrente_RMS']
	valor = escala_tensao_RMS * escala_corrente_RMS
	energia = 0
	segundo_hora = 0
	acumulador = 0
	segundo = 0
	minuto = 1
	rele.write(1)
	while segundo_hora <= 3600:
		tempo = datetime.now()
		if tempo.second == segundo:
			#print segundo
			minuto += 1.0
			potencia_ativa = Medida('Potencia Ativa')
			acumulador = potencia_ativa.medida() * valor
			#print acumulador
			energia += acumulador * (1 / 3600.0) * (1 / 1000.0)
			#print energia
			if (tempo.second == 59):
				segundo = 0
				#print energia
			else:
				segundo += 1
				#print energia
			segundo_hora += 1
			#print segundo_hora
			print energia
		tempo = datetime.now()
	rele.write(0)
	return energia
		
def tarifa_segundo_hora():
	tarifa = json.loads(open('tarifa.json').read())
        escala = json.loads(open('escala.json').read())
        escala_tensao_RMS = escala['Tensao_RMS']
        escala_corrente_RMS = escala['Corrente_RMS']
        escala_pot = escala_tensao_RMS * escala_corrente_RMS
        energia = 0
        segundo_hora = 0
        acumulador = 0
        segundo = 0
	rele.write(1)
        while segundo_hora <= 3600:
                tempo = datetime.now()
                semana = datetime.today().weekday()
		if tempo.second == segundo:
                        potencia_ativa = Medida('Potencia Ativa')
                        acumulador = potencia_ativa.medida() * escala_pot
                        energia += acumulador * (1 / 3600.0) * (1 / 1000.0)
			
                        if (tempo.second == 59):
                                segundo = 0
                                #print energia
                        else:
                                segundo += 1
                                #print energia
                        segundo_hora += 1
			
                        if tempo.hour < 17 or tempo.hour > 22 or semana >= 5:
                                fora_ponta = tarifa["fora_ponta"]
                                valor =  energia * (fora_ponta/1000)
                        elif tempo.hour > 18 or tempo.hour < 21 and semana < 5:
                                ponta = tarifa["ponta"]
                                valor = energia * (ponta/1000)
                        else:
                                inter = tarifa["intermediario"]
                                valor = energia_hora * (ponta / 1000)
			if  tempo.minute == 0 or tempo.minute == 15 or tempo.minute == 30 or tempo.minute == 45 and tempo.second == 0:
				tarifa_hora = ((energia),(valor),(tempo.hour),(tempo.minute),(tempo.second))
				out2 = csv.writer(file('tarifa_hora.csv', 'a'))
				out2.writerow(tarifa_hora)
				print 'passou aqui'
                tempo = datetime.now()
	rele.write(0)
        return valor

def tarifa_segundo_dia():
	i = 1
        tarifa = json.loads(open('tarifa.json').read())
        escala = json.loads(open('escala.json').read())
        escala_tensao_RMS = escala['Tensao_RMS']
        escala_corrente_RMS = escala['Corrente_RMS']
        escala_pot = escala_tensao_RMS * escala_corrente_RMS
        energia = 0
        segundo_hora = 0
        acumulador = 0
        segundo = 0
#	rele.write(1)
        while segundo_hora <= 86400:
                tempo = datetime.now()
		semana = datetime.today().weekday()
                if tempo.second == segundo:
                        potencia_ativa = Medida('Potencia Ativa')
                        acumulador = potencia_ativa.medida() * escala_pot
                        energia += acumulador * (1 / 3600.0) * (1 / 1000.0)

                        if (tempo.second == 59):
                                segundo = 0
                        else:
                                segundo += 1
                        segundo_hora += 1
                        print 'Energia'
                        print energia

                        cliente = paho.Client()
                        cliente.connect("iot.eclipse.org")

                        if tempo.hour < 17 or tempo.hour > 22 or semana >= 5:
                                fora_ponta = tarifa["fora_ponta"]
                                valor =  energia * (fora_ponta/1000)
                                print("Valor")
                                print(valor)
				tensao = Medida('Tensao RMS')
                                tensao_rede = tensao.medida()
                                valor_tensao = ((tensao_rede * escala_tensao_RMS) / 0.999999999)
                                corrente = Medida('Corrente RMS')
                                corrente_rede = corrente.medida()
                                valor_corrente = ((corrente_rede * escala_corrente_RMS)/0.999999999)
                                pot_at = Medida('Potencia Ativa')
                                potencia_ativa1 = pot_at.medida()
                                valor_potencia_ativa = ((potencia_ativa1 * (escala_corrente_RMS * escala_tensao_RMS))/ (0.999999999 * 0.999999999))
                                pot_ap = Medida('Potencia Aparente')
                                potencia_aparente = pot_ap.medida()
                                valor_potencia_aparente = ((potencia_aparente *  (escala_corrente_RMS * escala_tensao_RMS))/ (0.999999999 * 0.999999999))
                                fator = Medida('Fator Potencia')
                                fp = fator.medida()
                                payload =       {
                                        'tensao_rms':valor_tensao,
                                        'corrente_rms':valor_corrente,
                                        'potencia_ativa':valor_potencia_ativa,
                                        'potencia_aparente':valor_potencia_aparente,
                                        'fator_potencia':fp,
                                        'tarifa':valor
                                        }
                                lol = str(json.dumps(payload))
                                cliente.publish(topic="Dashboard/Device1",payload=lol)
                                print(lol)

                        elif tempo.hour > 18 or tempo.hour < 21 and semana < 5:
                                ponta = tarifa["ponta"]
                                valor = energia * (ponta/1000)
                                print("Valor")
                                print(valor)
				tensao = Medida('Tensao RMS')
                                tensao_rede = tensao.medida()
                                valor_tensao = ((tensao_rede * escala_tensao_RMS) / 0.999999999)
                                corrente = Medida('Corrente RMS')
                                corrente_rede = corrente.medida()
                                valor_corrente = ((corrente_rede * escala_corrente_RMS)/0.999999999)
                                pot_at = Medida('Potencia Ativa')
                                potencia_ativa1 = pot_at.medida()
                                valor_potencia_ativa = ((potencia_ativa1 * (escala_corrente_RMS * escala_tensao_RMS))/ (0.999999999 * 0.999999999))
                                pot_ap = Medida('Potencia Aparente')
                                potencia_aparente = pot_ap.medida()
                                valor_potencia_aparente = ((potencia_aparente *  (escala_corrente_RMS * escala_tensao_RMS))/ (0.999999999 * 0.999999999))
                                fator = Medida('Fator Potencia')
                                fp = fator.medida()
                                payload =       {
                                        'tensao_rms':valor_tensao,
                                        'corrente_rms':valor_corrente,
                                        'potencia_ativa':valor_potencia_ativa,
                                        'potencia_aparente':valor_potencia_aparente,
                                        'fator_potencia':fp,
                                        'tarifa':valor
                                        }
                                lol = str(json.dumps(payload))
                                cliente.publish(topic="Dashboard/Device1",payload=lol)
                                print(lol)
                        else:
                                inter = tarifa["intermediario"]
                                valor = energia_hora * (ponta / 1000)
                                print("Valor")
                                print(valor)
				tensao = Medida('Tensao RMS')
                                tensao_rede = tensao.medida()
                                valor_tensao = ((tensao_rede * escala_tensao_RMS) / 0.999999999)
                                corrente = Medida('Corrente RMS')
                                corrente_rede = corrente.medida()
                                valor_corrente = ((corrente_rede * escala_corrente_RMS)/0.999999999)
                                pot_at = Medida('Potencia Ativa')
                                potencia_ativa1 = pot_at.medida()
                                valor_potencia_ativa = ((potencia_ativa1 * (escala_corrente_RMS * escala_tensao_RMS))/ (0.999999999 * 0.999999999))
                                pot_ap = Medida('Potencia Aparente')
                                potencia_aparente = pot_ap.medida()
                                valor_potencia_aparente = ((potencia_aparente *  (escala_corrente_RMS * escala_tensao_RMS))/ (0.999999999 * 0.999999999))
                                fator = Medida('Fator Potencia')
                                fp = fator.medida()
                                payload =       {
                                        'tensao_rms':valor_tensao,
                                        'corrente_rms':valor_corrente,
                                        'potencia_ativa':valor_potencia_ativa,
                                        'potencia_aparente':valor_potencia_aparente,
                                        'fator_potencia':fp,
                                        'tarifa':valor
                                        }
                                lol = str(json.dumps(payload))
                                cliente.publish(topic="Dashboard/Device1",payload=lol)
                                print(lol)
			cliente.disconnect()
			print 'passei por aqui'
                tempo = datetime.now()
#	rele.write(0)
        return 1


def tarifa_segundo_mes():
	valor = 0
	data = datetime.now()
	if data.month == 1 or data.month == 3 or data.month == 5 or data.month ==7 or data.month == 8 or data.month == 10 or data.month == 12:
		mes = 31
		while mes > 0:
			valor = tarifa_segundo_dia()
			mes = mes - 1
	elif data.month == 2:
		mes = 28
		while mes > 0:
			valor = tarifa_segundo_dia()
			mes = mes - 1
	else:
		mes = 30
		while mes > 0:
			valor = tarifa_Segundo_dia()
			mes = mes - 1
	return 0

tv = tarifa_segundo_dia() 
print tv
