#!/usr/bin/python
import mraa
from Medida import *
from datetime import datetime
import json

global rele

rele = mraa.Gpio(4)
rele.dir(mraa.DIR_OUT)

############### Testar em breve ###################
# Montar uma main so para isso
def energia_segundo_hora():
	escala = json.loads(open('escala.json').read())
	escala_tensao_RMS = escala['Tensao_RMS']
	escala_corrente_RMS = escala['Corrente_RMS']
	valor = escala_tensao_RMS * escala_corrente_RMS
	energia = 0
	segundo_hora = 0
	acumulador = 0
	segundo = 0
	minuto = 1
	while segundo_hora <= 3600:
		tempo = datetime.now()
		if tempo.second == segundo:
			#print segundo
			minuto += 1.0
			potencia_ativa = Medida('Potencia Ativa')
			acumulador = potencia_ativa.medida() * valor
			#print acumulador
			energia += acumulador * (1 / 3600.0) * (1 / 1000.0)
			#print energia
			if (tempo.second == 59):
				segundo = 0
				#print energia
			else:
				segundo += 1
				#print energia
			segundo_hora += 1
			#print segundo_hora
			print energia
		tempo = datetime.now()
	return energia
		
def potencia_media_min():
	vi = True
	while vi:
		acumulador = 0
		a = 0
		n = datetime.now()
		if(n.second == 0):
			print n
			while (n.second != 59 and n.microsecond < 990000):
				potencia_ativa = Medida('Potencia Ativa')
				acumulador += potencia_ativa.medida()
				a += 1.0
				n = datetime.now()
			acumulador /= a
			print acumulador
			print a
			print n
			vi = False
	return acumulador

def energia_hora():
	r = datetime.now()
        acumulador1 = 0
        a = 60 - r.minute
	energia = 0
        for i in range(0,a):
                energia += potencia_media_min()
		print 'Energia: '
		print energia
        return energia

#def energia_min(self,min):
#	acumulador = 0
#	a = min
#	for i in range(0,min)
#		acumulador += potencia_media()
#	acumulador /= a
#	return acumulador * min

#def energia_dia(self):
#        acumulador = 0
#        for i in range(0,a):
#                acumulador += energia_hora()
#        acumulador /= a
#        return acumulador * 24

def tarifa():
	tarifa = json.loads(open('tarifa.json').read())
	tempo = datetime.now()
	if tempo.hour < 17 or tempo.hour > 22:
		fora_ponta = tarifa["fora_ponta"] 
		valor =  energia_segundo_hora() * (fora_ponta / 1000.0) * (1 / 1000.0)
		print("Valor")
		print(valor)
	elif tempo.hour > 18 or tempo.hour < 21:
		ponta = tarifa["ponta"]
		valor = energia_segundo_hora() * (ponta / 1000)
		print("Valor")
		print(valor)
	else:
		inter = tarifa["intermediario"]
		valor = energia_segundo_hora() * (ponta / 1000)
		print("Valor")
		printf(valor)
	return -1

