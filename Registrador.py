#!/usr/bin/python
import mraa 
import time
from Inicializacao import *
#SPI funciona como um carrosel
#Parte global necessaria para mexer com o SPI
global ss
global spi

#Classe que permite a leitura e a escrita dos registradores
class Registrador(object):
#Construtor
	def __init__(self,idReg,valor):
		self.idReg = idReg
		self.valor = valor
#Metodo para leitura de um registrador
	def leitura(self):

#Necessidade de passar os dados usando bytearray
#Dividir o valor em byte nao passando o valor inteiro
#No caso se passa 0xFF em valor
		txbuf = bytearray(4)
		txbuf[0] = (0x00 | (self.idReg << 1))
		txbuf[1] = self.valor[0]
		txbuf[2] = self.valor[1]
		txbuf[3] = self.valor[2]
#		for y in range(0,4):
#			print(txbuf[y])
		rxbuf = bytearray(4)
		ss.write(0)
#Envio e recebimento das informacoes do SPI
		for y in range(0,4):
			rxbuf[y] = spi.writeByte(txbuf[y])
		ss.write(1)
#		time.sleep(0.1)
#		for y in range(0,4):
#			print(rxbuf[y])
#Une os pedacos recebidos do SPI
		valor = 0
		valor = (rxbuf[1] << 16) | (rxbuf[2] << 8) | (rxbuf[3])
#		print valor
		return valor

#Mesma coisa que na parte de leitura
#Necessidade de bytearray e o valor precisa ser escrito em bytes divididos corretamente
	def escrita(self):
		self.idReg = (self.idReg << 1)
		self.idReg = (self.idReg | 0x40)
                txbuf = bytearray(4)
                txbuf[0] = self.idReg
                txbuf[1] = self.valor[0]
                txbuf[2] = self.valor[1]
                txbuf[3] = self.valor[2]
  #              for y in range(0,4):
  #                      print(txbuf[y])
                rxbuf = bytearray(4)
                ss.write(0)
                for y in range(0,4):
                        rxbuf[y] = spi.writeByte(txbuf[y])
                ss.write(1)
#                time.sleep(0.1)
 #               for y in range(0,4):
 #                       print(rxbuf[y])
                valor = (rxbuf[1] << 16) | (rxbuf[2] << 8) | (rxbuf[3])
#                return valor
