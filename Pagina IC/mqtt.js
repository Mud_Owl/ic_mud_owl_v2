    client = new Paho.MQTT.Client("iot.eclipse.org", Number(80),"/ws", "dash");
    // set callback handlers
    client.onConnectionLost = onConnectionLost;
    client.onMessageArrived = onMessageArrived;

    // connect the client
    client.connect({onSuccess:onConnect});

    // called when the client connects
    function onConnect() 
    {
        console.log("Connection Sucessfull")
        client.subscribe("dash/dev1");
    }

    // called when the client loses its connection
    function onConnectionLost(responseObject) {
        if (responseObject.errorCode !== 0) 
        {
            console.log("onConnectionLost:"+responseObject.errorMessage);
        }
    }

    // called when a message arrives
    function onMessageArrived(message) 
    {
        var res = JSON.parse(message.payloadString);
            
        document.getElementById("Corrente").innerHTML =res.corrente_rms;
        document.getElementById("Tensao").innerHTML =res.tensao_rms;
        document.getElementById("PotAtiva").innerHTML =res.potencia_ativa;
        document.getElementById("FatPot").innerHTML =res.fator_potencia;
        document.getElementById("PotApa").innerHTML = res.potencia_aparente;
        document.getElementById("Tarifa").innerHTML = res.tarifa;
    }
