#!/usr/bin/python
from datetime import datetime
import json
import mraa 
from Medida import *
from Dado import *
from Conversao import Conversao
from Registrador import *
from Inicializacao import *
from Comando import *
from Energia import *

global ss
global spi
global reset

spi = mraa.Spi(0)
spi.lsbmode(False)
spi.mode(mraa.SPI_MODE0)
spi.frequency(1000000)

ss = mraa.Gpio(10)
ss.dir(mraa.DIR_OUT)

reset = mraa.Gpio(8)
reset.dir(mraa.DIR_OUT)

ss.write(0)
reset.write(0)
time.sleep(0.1)
reset.write(1)
ss.write(1)

botao = mraa.Gpio(7)
botao.dir(mraa.DIR_IN)

#Parte de configuracao do CI
#Configuracao do registrador de configuracao
buf = bytearray(0)
txbuf = bytearray(3)
buf = 0
txbuf[0] = 0
txbuf[1] = 0
txbuf[2] = 1
valor = Registrador(buf,txbuf)
valor.escrita()
#Configuracao do registrador de mascara
buf1 = bytearray(0)
txbuf1 = bytearray(3)
buf1 = 26
txbuf1[0] = 0
txbuf1[1] = 0
txbuf1[2] = 0
valor1 = Registrador(buf1,txbuf1)
valor1.escrita()
#Configuracao do registrador de mode
buf2 = bytearray(0)
txbuf2 = bytearray(3)
buf2 = 18
txbuf2[0] = 0
txbuf2[1] = 0
txbuf2[2] = 97
valor2 = Registrador(buf2,txbuf2)
valor2.escrita()
#Configuracao do registrador de controle
buf3 = bytearray(0)
txbuf3 = bytearray(3)
buf3 = 28
txbuf3[0] = 0
txbuf3[1] = 0
txbuf3[2] = 4
valor3 = Registrador(buf3,txbuf3)
valor3.escrita()

cc = Comando('Ciclo continuo')
cc.escrita_comando()

#parte de calibracao
#calibracao TENSAO
estado = 0
contador = False
status = True
while status == True:
	if botao.read() == 1:
		estado += 1
		contador = True
	elif estado == 1 and contador == True:
		cal1 = Comando('Vdc offset')
		cal1.escrita_comando()
		cal2 = Comando('Vac offset')
		cal2.escrita_comando()

		vdcoff = Medida('VdCoff')
		vdcoff1 = vdcoff.medida()
		print 'Tensao DC OFFSET'
		print vdcoff1
		vacoff = Medida('VAcOff')
		vacoff1 = vacoff.medida()
		print 'Tensao AC OFFSET'
		print vacoff1

		contador = False
	elif estado == 2 and contador == True:
		cal3 = Comando('Vdc gain')
		cal3.escrita_comando()
		cal4 = Comando('Vac gain')
		cal4.escrita_comando()

		vgn = Medida('Vgn')
		vgn1 = vgn.medida()
		print 'Tensao GANHO'
		print vgn1

		contador = False
	elif estado == 3 and contador == True:
		cal5 = Comando('Vdc offset')
		cal5.escrita_comando()
		cal6 = Comando('Vac offset')
		cal6.escrita_comando()

		vdcoff2 = Medida('VdCoff')
		vdcoff3 = vdcoff2.medida()
		print 'Tensao DC OFFSET'
		print vdcoff3
		vacoff2 = Medida('VAcOff')
		vacoff3 = vacoff2.medida()
		print 'Tensao AC OFFSET'
		print vacoff3

		contador = False
		status =  False
	else:
		print '*'

#parte de calibracao
#calibracao CORRENTE

estado = 0
contador = False
status =  True
while True:
	if botao.read() == 1:
		estado += 1
		contador = True
	elif estado == 1 and contador == True:
		cal1 = Comando('Idc offset')
		cal1.escrita_comando()
		cal2 = Comando('Iac offset')
		cal2.escrita_comando()

		idcoff = Medida('IdCoff')
		idcoff1 = idcoff.medida()
		print 'Corrente DC OFFSET'
		print idcoff1
		iacoff = Medida('IAcOff')
		iacoff1 = iacoff.medida()
		print 'Corrente AC OFFSET'
		print iacoff1

		contador = False
	elif estado == 2 and contador == True:
		cal3 = Comando('Idc gain')
		cal3.escrita_comando()
		cal4 = Comando('Iac gain')
		cal4.escrita_comando()

		ign = Medida('Ign')
		ign1 = ign.medida()
		print 'Corrente GANHO'
		print ign1

		contador = False
	elif estado == 3 and contador == True:
		cal5 = Comando('Idc offset')
		cal5.escrita_comando()
		cal6 = Comando('Iac offset')
		cal6.escrita_comando()

		idcoff2 = Medida('IdCoff')
		idcoff3 = idcoff2.medida()
		print 'Corrente DC OFFSET'
		print idcoff3
		iacoff2 = Medida('IAcOff')
		iacoff3 = iacoff2.medida()
		print 'Corrente AC OFFSET'
		print iacoff3

		contador = False
		status =  False
	else:
		print '*'
