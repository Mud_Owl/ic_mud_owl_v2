#!/usr/bin/python
#Gambiarra para permitir que haja visualizacao global
#Declaracao global dos pinos e do SPI

import mraa 
import time 
from Registrador import *

global ss
global spi
global reset
global rele

#Testar
#def inicializacao():
spi = mraa.Spi(0)
spi.lsbmode(False)
spi.mode(mraa.SPI_MODE0)
spi.frequency(1000000)

ss = mraa.Gpio(10)
ss.dir(mraa.DIR_OUT)

reset = mraa.Gpio(8)
reset.dir(mraa.DIR_OUT)

ss.write(0)
reset.write(0)
time.sleep(0.1)
reset.write(1)
ss.write(1)

rele = mraa.Gpio(4)
rele.dir(mraa.DIR_OUT)

print 'passou por aqui'


